package com.learning.banksystem.repository;

import com.learning.banksystem.dto.BankLogResponse;
import com.learning.banksystem.model.BankLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface BankLogRepository extends JpaRepository<BankLog, Integer> {

    @Query("select new " +
            "com.learning.banksystem.dto.BankLogResponse" +
            "(b.id, b.operationType, b.operationDescription, b.recipient.id, b.sender.id, b.dateAndTime) " +
            "from BankLog b where b.recipient.id = ?1 or b.sender.id = ?1")
    List<BankLogResponse> getAllByClientAccountId(int clientAccountId);

    @Query("select new " +
            "com.learning.banksystem.dto.BankLogResponse" +
            "(b.id, b.operationType, b.operationDescription, b.recipient.id, b.sender.id, b.dateAndTime) " +
            "from BankLog b where (b.recipient.id = ?1 or b.sender.id =?1) and (b.dateAndTime between ?2 and ?3)")
    List<BankLogResponse> getAllByClientAccountIdAndDateRange(int clientAccountId, LocalDateTime from, LocalDateTime to);

    @Query("select distinct new " +
            "com.learning.banksystem.dto.BankLogResponse" +
            "(b.id, b.operationType, b.operationDescription, b.recipient.id, b.sender.id, b.dateAndTime) " +
            "from BankLog b join ClientAccount ca on (b.recipient.id = ca.id or b.sender.id = ca.id) and ca.owner.id = ?1")
    Set<BankLogResponse> getAllByClientId(int clientId);

    @Query("select distinct new " +
            "com.learning.banksystem.dto.BankLogResponse" +
            "(b.id, b.operationType, b.operationDescription, b.recipient.id, b.sender.id, b.dateAndTime) " +
            "from BankLog b join ClientAccount ca on (b.recipient.id = ca.id or b.sender.id = ca.id) and ca.owner.id = ?1 " +
            "and (b.dateAndTime between ?2 and ?3)")
    Set<BankLogResponse> getAllByClientIdAndDateRange(int clientId, LocalDateTime from, LocalDateTime to);

    @Modifying
    @Query(value = "insert into " +
            "bank_log(operation_type, operation_description, recipient, sender, date_and_time)" +
            "values(?1, ?2, ?3, ?4, ?5)", nativeQuery = true)
    void saveLog(int type, String description, Integer recipient, Integer sender, LocalDateTime dateTime);
}
