package com.learning.banksystem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientAccountResponse {
    private int id;
    private int amount;
}
