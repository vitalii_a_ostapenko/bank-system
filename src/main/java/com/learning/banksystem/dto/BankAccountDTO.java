package com.learning.banksystem.dto;

import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class BankAccountDTO {

    @Min(value = 1)
    int amount;
}
