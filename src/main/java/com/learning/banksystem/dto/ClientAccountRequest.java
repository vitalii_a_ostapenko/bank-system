package com.learning.banksystem.dto;

import lombok.Data;

import javax.validation.constraints.Min;

@Data
public class ClientAccountRequest {
    @Min(0)
    private int amount;
}
