package com.learning.banksystem.dto;

import com.learning.banksystem.model.OperationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BankLogResponse {
    private int id;
    private OperationType operationType;
    private String operationDescription;
    private Integer recipient;
    private Integer sender;
    private LocalDateTime dateAndTime;
}
