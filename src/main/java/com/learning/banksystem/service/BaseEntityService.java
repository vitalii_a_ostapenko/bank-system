package com.learning.banksystem.service;

public interface BaseEntityService<T, ID> {
    T getOne(ID id);
}
