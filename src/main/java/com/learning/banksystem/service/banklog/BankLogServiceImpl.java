package com.learning.banksystem.service.banklog;

import com.learning.banksystem.dto.BankLogResponse;
import com.learning.banksystem.model.OperationType;
import com.learning.banksystem.repository.BankLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import static com.learning.banksystem.model.OperationType.ACCOUNT_TRANSFER_HISTORY;
import static com.learning.banksystem.model.OperationType.CLIENT_TRANSFER_HISTORY;

@Service
@Transactional
public class BankLogServiceImpl implements BankLogService {

    @Autowired
    private BankLogRepository bankLogRepository;

    @Override
    public List<BankLogResponse> getClientAccountLog(int accountId) {
        List<BankLogResponse> bankLogs = bankLogRepository.getAllByClientAccountId(accountId);
        save(ACCOUNT_TRANSFER_HISTORY, "all by account id: " + accountId, null, null);
        return bankLogs;
    }

    @Override
    public List<BankLogResponse> getClientAccountLogByDateRange(int accountId, LocalDateTime from, LocalDateTime to) {
        List<BankLogResponse> bankLogs = bankLogRepository.getAllByClientAccountIdAndDateRange(accountId, from, to);
        save(ACCOUNT_TRANSFER_HISTORY, "all by account id: " + accountId, null, null);
        return bankLogs;
    }

    @Override
    public Set<BankLogResponse> getClientLog(int clientId) {
        Set<BankLogResponse> bankLogs = bankLogRepository.getAllByClientId(clientId);
        save(CLIENT_TRANSFER_HISTORY, "all by client id: " + clientId, null, null);
        return bankLogs;
    }

    @Override
    public Set<BankLogResponse> getClientLogByDateRange(int clientId, LocalDateTime from, LocalDateTime to) {
        Set<BankLogResponse> bankLogs = bankLogRepository.getAllByClientIdAndDateRange(clientId, from, to);
        save(CLIENT_TRANSFER_HISTORY, "all by client id: " + clientId, null, null);
        return bankLogs;
    }

    @Override
    public void save(OperationType type, String description, Integer recipient, Integer sender) {
        bankLogRepository.saveLog(type.ordinal(), description, recipient, sender, LocalDateTime.now());
    }
}
