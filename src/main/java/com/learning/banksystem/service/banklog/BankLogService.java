package com.learning.banksystem.service.banklog;

import com.learning.banksystem.dto.BankLogResponse;
import com.learning.banksystem.model.OperationType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public interface BankLogService {

    List<BankLogResponse> getClientAccountLog(int accountId);
    List<BankLogResponse> getClientAccountLogByDateRange(int accountId, LocalDateTime from, LocalDateTime to);
    Set<BankLogResponse> getClientLog(int clientId);
    Set<BankLogResponse> getClientLogByDateRange(int clientId, LocalDateTime from, LocalDateTime to);
    void save(OperationType type, String description, Integer recipient, Integer sender);
}
