package com.learning.banksystem.service.bankaccount;

import com.learning.banksystem.model.BankAccount;
import com.learning.banksystem.service.BaseEntityService;

public interface BankAccountService extends BaseEntityService<BankAccount, Integer> {
    BankAccount moneyEmission(int amount);
}
