package com.learning.banksystem.service.bankaccount;

import com.learning.banksystem.model.BankAccount;
import com.learning.banksystem.repository.BankAccountRepository;
import com.learning.banksystem.service.banklog.BankLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.learning.banksystem.model.OperationType.MONEY_EMISSION;

@Service
public class BankAccountServiceImpl implements BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankLogService bankLogService;

    @Value("${bank-account-id}")
    private int bankAccountId;

    @Override
    public BankAccount getOne(Integer id) {
        return bankAccountRepository.getOne(id);
    }

    @Transactional
    @Override
    public BankAccount moneyEmission(int amount) {
        BankAccount bankAccount = bankAccountRepository.findById(bankAccountId)
                .orElseGet(() -> {
                    BankAccount account = new BankAccount();
                    account.setId(bankAccountId);
                    account.setAmount(amount);
                    return account;
                });
        bankAccount.setAmount(bankAccount.getAmount() + amount);
        bankAccountRepository.save(bankAccount);
        bankLogService.save(MONEY_EMISSION, "emission amount: " + amount, null, null);
        return bankAccount;
    }
}
