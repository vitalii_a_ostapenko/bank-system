package com.learning.banksystem.service.clientaccount;

import com.learning.banksystem.exception.NotEnoughMoneyException;
import com.learning.banksystem.model.ClientAccount;
import com.learning.banksystem.repository.ClientAccountRepository;
import com.learning.banksystem.service.banklog.BankLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.learning.banksystem.model.OperationType.MONEY_TRANSFER;

@Service
public class ClientAccountServiceImpl implements ClientAccountService {

    @Autowired
    private ClientAccountRepository clientAccountRepository;

    @Autowired
    private BankLogService bankLogService;

    @Override
    public ClientAccount getOne(Integer id) {
        return clientAccountRepository.getOne(id);
    }

    @Transactional
    @Override
    public void moneyTransfer(int senderId, int recipientId, int amount) {
        ClientAccount senderAccount = clientAccountRepository.getOne(senderId);
        ClientAccount recipientAccount = clientAccountRepository.getOne(recipientId);
        if (senderAccount.getAmount() < amount) {
            throw new NotEnoughMoneyException("sender account have not enough money to transfer");
        }
        senderAccount.setAmount(senderAccount.getAmount() - amount);
        recipientAccount.setAmount(recipientAccount.getAmount() + amount);
        bankLogService.save(MONEY_TRANSFER, "amount: " + amount, recipientId, senderId);
    }
}
