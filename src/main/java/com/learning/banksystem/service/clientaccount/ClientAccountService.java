package com.learning.banksystem.service.clientaccount;

import com.learning.banksystem.model.ClientAccount;
import com.learning.banksystem.service.BaseEntityService;

public interface ClientAccountService extends BaseEntityService<ClientAccount, Integer> {
    void moneyTransfer(int senderId, int recipientId, int amount);
}
