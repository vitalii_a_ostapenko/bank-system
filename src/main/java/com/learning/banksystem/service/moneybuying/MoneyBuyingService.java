package com.learning.banksystem.service.moneybuying;

import com.learning.banksystem.dto.ClientAccountResponse;

public interface MoneyBuyingService {
    ClientAccountResponse buyMoney(int clientAccountId, int amount);
}
