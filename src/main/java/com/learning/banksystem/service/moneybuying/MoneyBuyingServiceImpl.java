package com.learning.banksystem.service.moneybuying;

import com.learning.banksystem.dto.ClientAccountResponse;
import com.learning.banksystem.exception.NotEnoughMoneyException;
import com.learning.banksystem.model.BankAccount;
import com.learning.banksystem.model.ClientAccount;
import com.learning.banksystem.service.bankaccount.BankAccountService;
import com.learning.banksystem.service.banklog.BankLogService;
import com.learning.banksystem.service.clientaccount.ClientAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.learning.banksystem.model.OperationType.MONEY_BUYING;

@Service
public class MoneyBuyingServiceImpl implements MoneyBuyingService {

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private ClientAccountService clientAccountservice;

    @Autowired
    private BankLogService bankLogService;

    @Value("${bank-account-id}")
    private int bankAccountId;

    @Transactional
    @Override
    public ClientAccountResponse buyMoney(int clientAccountId, int amount) {
        BankAccount bankAccount = bankAccountService.getOne(bankAccountId);
        ClientAccount clientAccount = clientAccountservice.getOne(clientAccountId);
        if (bankAccount.getAmount() < amount) {
            throw new NotEnoughMoneyException("Bank doesn't have enough money to sell");
        }
        bankAccount.setAmount(bankAccount.getAmount() - amount);
        clientAccount.setAmount(clientAccount.getAmount() + amount);
        String description = String.format("bought: %d", amount);
        bankLogService.save(MONEY_BUYING, description, clientAccountId, null);
        return new ClientAccountResponse(clientAccount.getId(), clientAccount.getAmount());
    }
}
