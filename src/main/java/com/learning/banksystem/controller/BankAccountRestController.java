package com.learning.banksystem.controller;

import com.learning.banksystem.dto.BankAccountDTO;
import com.learning.banksystem.model.BankAccount;
import com.learning.banksystem.service.bankaccount.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/bank-account", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class BankAccountRestController {

    @Autowired
    private BankAccountService bankAccountService;

    @PatchMapping
    public ResponseEntity<BankAccount> moneyEmission(@Valid @RequestBody BankAccountDTO dto) {
        BankAccount bankAccount = bankAccountService.moneyEmission(dto.getAmount());
        return new ResponseEntity<>(bankAccount, HttpStatus.ACCEPTED);
    }
}
