package com.learning.banksystem.controller;

import com.learning.banksystem.dto.ClientAccountRequest;
import com.learning.banksystem.dto.ClientAccountResponse;
import com.learning.banksystem.service.moneybuying.MoneyBuyingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/money-buying", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MoneyBuyingRestController {

    @Autowired
    private MoneyBuyingService moneyBuyingService;

    @PatchMapping("/{id}")
    public ResponseEntity<ClientAccountResponse> buyMoney(@PathVariable int id,
                                                          @Valid @RequestBody ClientAccountRequest request) {
        ClientAccountResponse clientAccount = moneyBuyingService.buyMoney(id, request.getAmount());
        return new ResponseEntity<>(clientAccount, HttpStatus.ACCEPTED);
    }
}
