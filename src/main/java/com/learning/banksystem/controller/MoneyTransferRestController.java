package com.learning.banksystem.controller;

import com.learning.banksystem.dto.ClientAccountRequest;
import com.learning.banksystem.service.clientaccount.ClientAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/money-transfer", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class MoneyTransferRestController {

    @Autowired
    private ClientAccountService clientAccountService;

    @PatchMapping("/{senderId}/{recipientId}")
    public ResponseEntity transferMoney(@PathVariable int senderId,
                                        @PathVariable int recipientId,
                                        @Valid @RequestBody ClientAccountRequest request) {
        clientAccountService.moneyTransfer(senderId, recipientId, request.getAmount());
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }
}
