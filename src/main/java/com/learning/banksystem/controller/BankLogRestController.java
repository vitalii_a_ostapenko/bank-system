package com.learning.banksystem.controller;

import com.learning.banksystem.dto.BankLogResponse;
import com.learning.banksystem.service.banklog.BankLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/bank-log", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class BankLogRestController {

    @Autowired
    private BankLogService bankLogService;

    @GetMapping(params = "clientAccountId")
    public ResponseEntity<List<BankLogResponse>> getAllByClientAccountId(@RequestParam int clientAccountId) {
        List<BankLogResponse> bankLogs = bankLogService.getClientAccountLog(clientAccountId);
        return new ResponseEntity<>(bankLogs, HttpStatus.OK);
    }

    @GetMapping(params = {"clientAccountId", "from", "to"})
    public ResponseEntity<List<BankLogResponse>>
    getAllByClientAccountIdAndDateRange(@RequestParam int clientAccountId,
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate from,
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate to) {
        List<BankLogResponse> bankLogs = bankLogService
                .getClientAccountLogByDateRange(clientAccountId, from.atStartOfDay(), to.atTime(23, 59, 59, 59));
        return new ResponseEntity<>(bankLogs, HttpStatus.OK);
    }

    @GetMapping(params = "clientId")
    public ResponseEntity<Set<BankLogResponse>> getAllByClientId(@RequestParam int clientId) {
        Set<BankLogResponse> bankLogs = bankLogService.getClientLog(clientId);
        return new ResponseEntity<>(bankLogs, HttpStatus.OK);
    }

    @GetMapping(params = {"clientId", "from", "to"})
    public ResponseEntity<Set<BankLogResponse>>
    getAllByClientIdAndDateRange(@RequestParam int clientId,
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate from,
                                        @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate to) {
        Set<BankLogResponse> bankLogs = bankLogService
                .getClientLogByDateRange(clientId, from.atStartOfDay(), to.atTime(23, 59, 59, 59));
        return new ResponseEntity<>(bankLogs, HttpStatus.OK);
    }
}
