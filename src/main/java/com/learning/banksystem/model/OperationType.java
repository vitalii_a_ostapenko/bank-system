package com.learning.banksystem.model;

public enum OperationType {
    MONEY_EMISSION, MONEY_BUYING, MONEY_TRANSFER, ACCOUNT_TRANSFER_HISTORY, CLIENT_TRANSFER_HISTORY
}
