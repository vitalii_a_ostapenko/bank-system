package com.learning.banksystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "bank_account")
@Data
public class BankAccount extends BaseEntity {
    private int amount;
}
