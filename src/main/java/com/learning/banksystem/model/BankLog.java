package com.learning.banksystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "bank_log")
@Data
public class BankLog extends BaseEntity {

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "operation_type")
    private OperationType operationType;

    @Column(name = "operation_description", columnDefinition = "VARCHAR")
    private String operationDescription;

    @ManyToOne
    @JoinColumn(name = "recipient")
    private ClientAccount recipient;

    @ManyToOne
    @JoinColumn(name = "sender")
    private ClientAccount sender;

    @Column(name = "date_and_time")
    private LocalDateTime dateAndTime;
}
