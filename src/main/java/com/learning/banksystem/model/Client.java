package com.learning.banksystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "client")
@Data
public class Client extends BaseEntity {

    @Column(columnDefinition = "VARCHAR(64)")
    private String name;

    @Column(columnDefinition = "VARCHAR(13)")
    private String phone;
}
